package hzt.model.attribute;

import hzt.model.perlin_noise.NoiseEngine;
import javafx.scene.paint.Color;

public class BrightnessAttribute extends Attribute {

    private static final int MAX_BRIGHTNESS = 1;

    @Override
    public Color getColorAttribute(Color color, double noise) {
        noise = NoiseEngine.map(noise, 0, MAX_BRIGHTNESS);
        return Color.hsb(color.getHue(), color.getSaturation(), noise);
    }

    @Override
    public String toString() {
        return "Brightness";
    }


}
