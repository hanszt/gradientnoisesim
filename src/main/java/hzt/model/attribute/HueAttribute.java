package hzt.model.attribute;

import hzt.model.perlin_noise.NoiseEngine;
import javafx.scene.paint.Color;

public class HueAttribute extends Attribute {

    private static final int MAX_HUE = 360;

    @Override
    public Color getColorAttribute(Color color, double noise) {
        noise = NoiseEngine.map(noise, 0, MAX_HUE);
        return Color.hsb(noise, color.getSaturation(), color.getBrightness());
    }

    @Override
    public String toString() {
        return "Hue";
    }


}
