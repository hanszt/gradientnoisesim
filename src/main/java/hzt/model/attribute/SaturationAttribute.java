package hzt.model.attribute;

import hzt.model.perlin_noise.NoiseEngine;
import javafx.scene.paint.Color;

public class SaturationAttribute extends Attribute {

    private static final int MAX_SATURATION = 1;

    @Override
    public Color getColorAttribute(Color color, double noise) {
        noise = NoiseEngine.map(noise, 0, MAX_SATURATION);
        return Color.hsb(color.getHue(), noise, color.getBrightness());
    }

    @Override
    public String toString() {
        return "Saturation";
    }


}
