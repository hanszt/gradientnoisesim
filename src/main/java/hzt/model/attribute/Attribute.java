package hzt.model.attribute;

import javafx.scene.paint.Color;

public abstract class Attribute {
    public Attribute() {
    }

    public abstract Color getColorAttribute(Color color, double noise);

    public abstract String toString();
}


