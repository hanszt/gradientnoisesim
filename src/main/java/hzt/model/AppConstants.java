package hzt.model;

import javafx.geometry.Dimension2D;
import javafx.geometry.Point3D;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import lombok.Getter;

import static javafx.scene.paint.Color.NAVY;
import static javafx.scene.paint.Color.ORANGE;

public interface AppConstants {

    int INIT_FRAME_RATE = 30; // f/s

    double INIT_GRADIENT_NOISE_OFFSET_Z = 20.0;

    Duration INIT_FRAME_DURATION = Duration.seconds(1. / INIT_FRAME_RATE); // s/f
    Point3D INIT_GRADIENT_INCREMENT_VECTOR = new Point3D(.05, .05, .01);

    Color INIT_BG_COLOR = NAVY;
    Color INIT_GRID_COLOR = ORANGE;

    Dimension2D MIN_STAGE_DIMENSION = new Dimension2D(750, 500);
    Dimension2D INIT_SCENE_DIMENSION = new Dimension2D(1200, 800);
    Dimension2D INIT_ANIMATION_PANE_DIMENSION = new Dimension2D(640, 640);
    Dimension2D INIT_GRID_SIZE = INIT_ANIMATION_PANE_DIMENSION;

    Dimension2D INIT_RECTANGLE_GRID_RESOLUTION = new Dimension2D(120, 120);
    Dimension2D INIT_ELLIPSE_GRID_RESOLUTION = new Dimension2D(50, 50);
    Dimension2D INIT_ROTATIONAL_GRID_RESOLUTION = new Dimension2D(40, 40);

    String ANSI_RESET = "\u001B[0m";
    String ANSI_BLUE = "\u001B[34m";
    String TITLE = "Gradient Noise Visualisation";
    String DOTTED_LINE = "----------------------------------------------------------------------------------------\n";
    String CLOSING_MESSAGE = ANSI_BLUE + "See you next Time! :)" + ANSI_RESET;
    float STAGE_OPACITY = 0.8f;

    @Getter
    enum Scene {

        MAIN_SCENE("mainScene.fxml"),
        ABOUT_SCENE("aboutScene.fxml");

        private final String fxmlFileName;

        Scene(String fxmlFileName) {
            this.fxmlFileName = fxmlFileName;
        }

        public String formattedName() {
            return this.name().charAt(0) + this.name().substring(1).toLowerCase().replace("_", " ");
        }

    }

}
