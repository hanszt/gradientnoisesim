package hzt.model.perlin_noise;

/**
 * all noise functions return a value between -1 and 1
 */
public interface NoiseEngine {

    double MAX_NOISE_VAL = 1;
    double MIN_NOISE_VAL = -1;

    double noise(double xOff, double yOff);

    double noise(double xOff, double yOff, double zOff);

    /*
     * Utility functions
     */
    static int fastFloor(double x) {
        int xi = (int) x;
        return (x < xi) ? (xi - 1) : xi;
    }

    static double map(double curValue, double newLower, double newUpper) {
        double curRange = MAX_NOISE_VAL - MIN_NOISE_VAL;
        double newRange = newUpper - newLower;
        return ((curValue - MIN_NOISE_VAL) * (newRange / curRange)) + newLower;
    }

    static double lerp(double curValue, double lowerBound, double upperBound) {
        return lowerBound + curValue * (upperBound - lowerBound);
    }

    static double sCurve(double curValue) {
        return curValue * curValue * (3 - curValue - curValue);
    }


    String toString();
}
