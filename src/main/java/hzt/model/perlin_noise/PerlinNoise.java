package hzt.model.perlin_noise;

import static hzt.model.perlin_noise.NoiseEngine.*;

/**
 * Copyright 2001 Ken Perlin
 * <p>
 * Computes Perlin Noise for one, two, and three dimensions.<p>
 * The result is a continuous function that interpolates a smooth path
 * along a series random points. The function is consistent, so given
 * the same parameters, it will always return the same value.
 */
public final class PerlinNoise implements NoiseEngine {

    /**
     * Initialization seed used to start the random number generator.
     */
    public static final int SEED = 100;

    private static final int P = 8;
    private static final int B = 1 << P;
    private static final int M = B - 1;

    private static final int NP = 8;
    private static final int N = 1 << NP;

    private static final int[] P_ARRAY = new int[B + B + 2];
    private static final double[][] GRID2 = new double[B + B + 2][2];
    private static final double[] GRID1 = new double[B + B + 2];
    private static final double[][] POINTS = new double[32][3];

    static {
        init();
    }


    /**
     * Computes noise function for one dimension at x.
     *
     * @param x 1 dimensional parameter
     * @return the noise value at x
     */
    public double noise(double x) {
        double t = x + N;
        int bx0 = ((int) t) & M;
        int bx1 = (bx0 + 1) & M;
        double rx0 = t - (int) t;
        double rx1 = rx0 - 1;

        double sx = sCurve(rx0);
        double u = rx0 * GRID1[P_ARRAY[bx0]];
        double v = rx1 * GRID1[P_ARRAY[bx1]];

        return lerp(sx, u, v);
    }

    /**
     * Computes noise function for two dimensions at the point (x,y).
     *
     * @param x x dimension parameter
     * @param y y dimension parameter
     * @return the value of noise at the point (x,y)
     */
    @Override
    public double noise(double x, double y) {
        double t = x + N;
        int bx0 = ((int) t) & M;
        int bx1 = (bx0 + 1) & M;
        double rx0 = t - (int) t;
        double rx1 = rx0 - 1;

        t = y + N;
        int by0 = ((int) t) & M;
        int by1 = (by0 + 1) & M;
        double ry0 = t - (int) t;
        double ry1 = ry0 - 1;

        int i = P_ARRAY[bx0];
        int j = P_ARRAY[bx1];

        int b00 = P_ARRAY[i + by0];
        int b10 = P_ARRAY[j + by0];
        int b01 = P_ARRAY[i + by1];
        int b11 = P_ARRAY[j + by1];

        double sx = sCurve(rx0);
        double sy = sCurve(ry0);

        double[] q = GRID2[b00];
        double u = rx0 * q[0] + ry0 * q[1];
        q = GRID2[b10];
        double v = rx1 * q[0] + ry0 * q[1];
        double a = lerp(sx, u, v);

        q = GRID2[b01];
        u = rx0 * q[0] + ry1 * q[1];
        q = GRID2[b11];
        v = rx1 * q[0] + ry1 * q[1];
        double b = lerp(sx, u, v);

        return lerp(sy, a, b);
    }

    /**
     * Computes noise function for three dimensions at the point (x,y,z).
     *
     * @param x x dimension parameter
     * @param y y dimension parameter
     * @param z z dimension parameter
     * @return the noise value at the point (x, y, z)
     */
    @Override
    public double noise(double x, double y, double z) {

        int bx = (int) Math.IEEEremainder(Math.floor(x), B);
        if (bx < 0)
            bx += B;
        double rx0 = x - Math.floor(x);
        double rx1 = rx0 - 1;

        int by = (int) Math.IEEEremainder(Math.floor(y), B);
        if (by < 0)
            by += B;
        double ry0 = y - Math.floor(y);
        double ry1 = ry0 - 1;

        int bz = (int) Math.IEEEremainder(Math.floor(z), B);
        if (bz < 0)
            bz += B;
        double rz = z - Math.floor(z);

        int b0 = P_ARRAY[bx];

        bx++;

        int b1 = P_ARRAY[bx];

        int b00 = P_ARRAY[b0 + by];
        int b10 = P_ARRAY[b1 + by];

        by++;

        int b01 = P_ARRAY[b0 + by];
        int b11 = P_ARRAY[b1 + by];

        double sx = sCurve(rx0);
        double sy = sCurve(ry0);
        double sz = sCurve(rz);

        double[] q = g(b00 + bz);
        double u = rx0 * q[0] + ry0 * q[1] + rz * q[2];
        q = g(b10 + bz);
        double v = rx1 * q[0] + ry0 * q[1] + rz * q[2];
        double a = lerp(sx, u, v);
        q = g(b01 + bz);
        u = rx0 * q[0] + ry1 * q[1] + rz * q[2];
        q = g(b11 + bz);
        v = rx1 * q[0] + ry1 * q[1] + rz * q[2];
        double b = lerp(sx, u, v);
        double c = lerp(sy, a, b);
        bz++;
        rz--;
        q = g(b00 + bz);
        u = rx0 * q[0] + ry0 * q[1] + rz * q[2];
        q = g(b10 + bz);
        v = rx1 * q[0] + ry0 * q[1] + rz * q[2];
        a = lerp(sx, u, v);
        q = g(b01 + bz);
        u = rx0 * q[0] + ry1 * q[1] + rz * q[2];
        q = g(b11 + bz);
        v = rx1 * q[0] + ry1 * q[1] + rz * q[2];
        b = lerp(sx, u, v);
        double d = lerp(sy, a, b);

        return lerp(sz, c, d);
    }

    private static double[] g(int i) {
        return POINTS[i % 32];
    }

    private static void init() {
        int i, j, k;
        double u, v, w, U, V, W, Hi, Lo;
        java.util.Random r = new java.util.Random(SEED);
        for (i = 0; i < B; i++) {
            P_ARRAY[i] = i;
            GRID1[i] = 2 * r.nextDouble() - 1;

            do {
                u = 2 * r.nextDouble() - 1;
                v = 2 * r.nextDouble() - 1;
            } while (u * u + v * v > 1 || Math.abs(u) > 2.5 * Math.abs(v) || Math.abs(v) > 2.5 * Math.abs(u) || Math.abs(Math.abs(u) - Math.abs(v)) < .4);
            GRID2[i][0] = u;
            GRID2[i][1] = v;
            normalize2(GRID2[i]);

            do {
                u = 2 * r.nextDouble() - 1;
                v = 2 * r.nextDouble() - 1;
                w = 2 * r.nextDouble() - 1;
                U = Math.abs(u);
                V = Math.abs(v);
                W = Math.abs(w);
                Lo = Math.min(U, Math.min(V, W));
                Hi = Math.max(U, Math.max(V, W));
            } while (u * u + v * v + w * w > 1 || Hi > 4 * Lo || Math.min(Math.abs(U - V), Math.min(Math.abs(U - W), Math.abs(V - W))) < .2);
        }

        while (--i > 0) {
            k = P_ARRAY[i];
            j = (int) (r.nextLong() & M);
            P_ARRAY[i] = P_ARRAY[j];
            P_ARRAY[j] = k;
        }
        for (i = 0; i < B + 2; i++) {
            P_ARRAY[B + i] = P_ARRAY[i];
            GRID1[B + i] = GRID1[i];
            for (j = 0; j < 2; j++) {
                GRID2[B + i][j] = GRID2[i][j];
            }
        }

        POINTS[3][0] = POINTS[3][1] = POINTS[3][2] = Math.sqrt(1. / 3);
        double r2 = Math.sqrt(1. / 2);
        double s = Math.sqrt(2 + r2 + r2);

        for (i = 0; i < 3; i++)
            for (j = 0; j < 3; j++)
                POINTS[i][j] = (i == j ? 1 + r2 + r2 : r2) / s;
        for (i = 0; i <= 1; i++)
            for (j = 0; j <= 1; j++)
                for (k = 0; k <= 1; k++) {
                    int n = i + j * 2 + k * 4;
                    if (n > 0)
                        for (int m = 0; m < 4; m++) {
                            POINTS[4 * n + m][0] = (i == 0 ? 1 : -1) * POINTS[m][0];
                            POINTS[4 * n + m][1] = (j == 0 ? 1 : -1) * POINTS[m][1];
                            POINTS[4 * n + m][2] = (k == 0 ? 1 : -1) * POINTS[m][2];
                        }
                }
    }

    private static void normalize2(double v[]) {
        double s;
        s = Math.sqrt(v[0] * v[0] + v[1] * v[1]);
        v[0] = v[0] / s;
        v[1] = v[1] / s;
    }

    @Override
    public String toString() {
        return "Perlin Noise";
    }

}
