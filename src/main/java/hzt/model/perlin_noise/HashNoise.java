package hzt.model.perlin_noise;

import java.util.Objects;

public final class HashNoise implements NoiseEngine {

    @Override
    public double noise(double xOff, double yOff) {
        final var hash = (double) Objects.hash(xOff, yOff);
        return hash / Integer.MAX_VALUE;
    }

    @Override
    public double noise(double xOff, double yOff, double zOff) {
        final var hash = (double) Objects.hash(xOff, yOff, zOff);
        return hash / Integer.MAX_VALUE;
    }

    @Override
    public String toString() {
        return "Hash noise";
    }
}
