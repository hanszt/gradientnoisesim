package hzt.model.perlin_noise;

import java.util.Objects;
import java.util.Random;

public final class RandomNoise implements NoiseEngine {

    private static final Random RANDOM = new Random();

    @Override
    public double noise(double xOff, double yOff) {
        final var seed = Math.round(xOff + yOff);
        RANDOM.setSeed(seed);
        return RANDOM.nextDouble() * 2 - 1;
    }

    @Override
    public double noise(double xOff, double yOff, double zOff) {
        final var seed = Objects.hash(xOff, yOff, zOff);
        RANDOM.setSeed(seed);
        return RANDOM.nextDouble() * 2 - 1;
    }

    @Override
    public String toString() {
        return "Random noise";
    }
}
