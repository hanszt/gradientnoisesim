package hzt.model;

import java.util.Objects;

public class Theme {

    private final String name;
    private final String fileName;

    public Theme(String name, String fileName) {
        this.name = name;
        this.fileName = fileName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Theme theme = (Theme) o;
        return Objects.equals(name, theme.name) && Objects.equals(fileName, theme.fileName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, fileName);
    }

    public String getName() {
        return name;
    }

    public String getFileName() {
        return fileName;
    }

    @Override
    public String toString() {
        return name;
    }
}
