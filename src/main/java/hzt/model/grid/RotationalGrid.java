package hzt.model.grid;

import javafx.geometry.Dimension2D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import static hzt.model.AppConstants.*;
import static hzt.model.perlin_noise.NoiseEngine.map;

public class RotationalGrid extends PixelGrid {

    private static final double ROTATE_DEG = 180;

    public RotationalGrid() {
        this(INIT_ROTATIONAL_GRID_RESOLUTION, INIT_GRID_SIZE);
    }

    public RotationalGrid(Dimension2D gridResolution, Dimension2D pixelSize) {
        super(gridResolution, pixelSize);
    }

    @Override
    Node createPixel(Dimension2D pixelSize, int row, int col) {
        Rectangle rotationalLine = new Rectangle();
        rotationalLine.setTranslateX(pixelSize.getWidth() * col);
        rotationalLine.setTranslateY(pixelSize.getHeight() * row);
        rotationalLine.setHeight(pixelSize.getHeight() / 8);
        rotationalLine.setWidth(pixelSize.getWidth());
        return rotationalLine;
    }

    @Override
    void updatePixelProperty(int row, int col, double xOff, double yOff, double zOff) {
        double noise = getCurrentNoiseEngine().noise(xOff, yOff, zOff);
        double angleBasedOnNoise = map(noise, -ROTATE_DEG, ROTATE_DEG);
        Rectangle rotationalLine = (Rectangle) getPixels().get(col).get(row);
        rotationalLine.setRotate(angleBasedOnNoise);
        Color color = getAttribute().getColorAttribute(this.getColor(), noise);
        rotationalLine.setFill(color);
    }

    @Override
    public String toString() {
        return "Rotational grid";
    }

}
