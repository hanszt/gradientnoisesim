package hzt.model.grid;

import javafx.geometry.Dimension2D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Ellipse;
import javafx.scene.shape.Shape;

import static hzt.model.AppConstants.INIT_ELLIPSE_GRID_RESOLUTION;
import static hzt.model.AppConstants.INIT_GRID_SIZE;

public class EllipsePixelGrid extends PixelGrid {

    public EllipsePixelGrid() {
        this(INIT_ELLIPSE_GRID_RESOLUTION, INIT_GRID_SIZE);
    }

    public EllipsePixelGrid(Dimension2D gridResolution, Dimension2D gridSize) {
        super(gridResolution, gridSize);
    }

    @Override
    Node createPixel(Dimension2D pixelSize, int row, int col) {
        Ellipse ellipse = new Ellipse();
        ellipse.setTranslateX(pixelSize.getWidth() * col);
        ellipse.setTranslateY(pixelSize.getHeight() * row);
        ellipse.setRadiusX(pixelSize.getWidth() / 2);
        ellipse.setRadiusY(pixelSize.getHeight() / 2);
       return ellipse;
    }

    @Override
    void updatePixelProperty(int row, int col, double xOff, double yOff, double zOff) {
        double noise  = getCurrentNoiseEngine().noise(xOff, yOff, zOff);
        Shape ellipsePixel = (Ellipse) getPixels().get(col).get(row);
        Color color = getAttribute().getColorAttribute(this.getColor(), noise);
        ellipsePixel.setFill(color);
    }

    @Override
    public String toString() {
        return "Ellipse grid";
    }

}
