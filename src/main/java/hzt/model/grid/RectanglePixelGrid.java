package hzt.model.grid;

import javafx.geometry.Dimension2D;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import static hzt.model.AppConstants.INIT_GRID_SIZE;
import static hzt.model.AppConstants.INIT_RECTANGLE_GRID_RESOLUTION;

public class RectanglePixelGrid extends PixelGrid {

    public RectanglePixelGrid() {
        this(INIT_RECTANGLE_GRID_RESOLUTION, INIT_GRID_SIZE);
    }

    public RectanglePixelGrid(Dimension2D gridResolution, Dimension2D gridSize) {
        super(gridResolution, gridSize);
    }

    @Override
    Node createPixel(Dimension2D pixelSize, int row, int col) {
        Rectangle rectangle = new Rectangle();
        rectangle.setX(pixelSize.getWidth() * col);
        rectangle.setY(pixelSize.getHeight() * row);
        rectangle.setWidth(pixelSize.getWidth());
        rectangle.setHeight(pixelSize.getHeight());
       return rectangle;
    }

    @Override
    void updatePixelProperty(int row, int col, double xOff, double yOff, double zOff) {
        double noise  = getCurrentNoiseEngine().noise(xOff, yOff, zOff);
        Rectangle rectanglePixel = (Rectangle) getPixels().get(col).get(row);
        Color color = getAttribute().getColorAttribute(this.getColor(), noise);
        rectanglePixel.setFill(color);
    }

    @Override
    public String toString() {
        return "Rectangle grid";
    }

}
