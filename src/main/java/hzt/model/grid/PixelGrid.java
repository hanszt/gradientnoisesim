package hzt.model.grid;

import hzt.model.Point3DProperty;
import hzt.model.attribute.Attribute;
import hzt.model.perlin_noise.NoiseEngine;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Dimension2D;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.paint.Color;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public abstract class PixelGrid implements Iterable<Node> {

    final Group gridContainer = new Group();
    private final ObjectProperty<Color> color = new SimpleObjectProperty<>();
    private final List<List<Node>> pixels = new ArrayList<>();

    private final ObjectProperty<Attribute> attribute = new SimpleObjectProperty<>();

    private final ObjectProperty<NoiseEngine> currentNoiseEngine = new SimpleObjectProperty<>();
    private final Point3DProperty increment = new Point3DProperty();

    private final DoubleProperty zOff = new SimpleDoubleProperty();

    protected PixelGrid(Dimension2D gridResolution, Dimension2D gridSize) {
        DoubleProperty gridWidth = new SimpleDoubleProperty();
        gridWidth.set(gridSize.getWidth());
        DoubleProperty gridHeight = new SimpleDoubleProperty();
        gridHeight.set(gridSize.getHeight());
        IntegerProperty horizontalResolution = new SimpleIntegerProperty();
        horizontalResolution.set((int) gridResolution.getWidth());
        IntegerProperty verticalResolution = new SimpleIntegerProperty();
        verticalResolution.set((int) gridResolution.getHeight());
        fillPixelGrid(gridResolution, gridSize);
    }

    private void fillPixelGrid(Dimension2D gridResolution, Dimension2D gridSize) {
        Dimension2D pixelSize = new Dimension2D(gridSize.getWidth() / gridResolution.getWidth(),
                gridSize.getHeight() / gridResolution.getHeight());
        for (int row = 0; row < gridResolution.getHeight(); row++) {
            List<Node> pixelColumn = new ArrayList<>();
            getPixels().add(pixelColumn);
            for (int col = 0; col < gridResolution.getWidth(); col++) {
                Node pixel = createPixel(pixelSize, row, col);
                pixelColumn.add(pixel);
                gridContainer.getChildren().add(pixel);
            }
        }
    }

    abstract Node createPixel(Dimension2D pixelSize, int row, int col);

    public void updateNoiseOffsetAllPixels() {
        updateNoiseOffsetAllPixels(increment.getX(), increment.getY(), increment.getZ());
    }

    public void updateNoiseXOffsetAllPixels(double xInc) {
        updateNoiseOffsetAllPixels(xInc, increment.getY(), increment.getZ());
    }

    public void updateNoiseYOffsetAllPixels(double yInc) {
        updateNoiseOffsetAllPixels(increment.getX(), yInc, increment.getZ());
    }

    private void updateNoiseOffsetAllPixels(double xInc, double yInc, double zInc) {
        int rows = pixels.get(0).size();
        int cols = pixels.size();
        double yOff = 0;
        for (int col = 0; col < cols; col++) {
            double xOff = 0;
            for (int row = 0; row < rows; row++) {
                updatePixelProperty(row, col, xOff, yOff, zOff.get());
                xOff += xInc;
            }
            yOff += yInc;
        }
        zOff.set(zOff.get() + zInc);
    }

    abstract void updatePixelProperty(int row, int col, double xOff, double yOff, double zOff);

    public void reset() {
    }

    @NotNull
    @Override
    public Iterator<Node> iterator() {
        return gridContainer.getChildren().iterator();
    }

    public Group getGridContainer() {
        return gridContainer;
    }

    public List<List<Node>> getPixels() {
        return pixels;
    }

    public Attribute getAttribute() {
        return attribute.get();
    }

    public ObjectProperty<Attribute> attributeProperty() {
        return attribute;
    }

    public Point3DProperty getIncrement() {
        return increment;
    }

    public ObjectProperty<NoiseEngine> currentNoiseEngineProperty() {
        return currentNoiseEngine;
    }

    public NoiseEngine getCurrentNoiseEngine() {
        return currentNoiseEngine.get();
    }

    public Color getColor() {
        return color.get();
    }

    public ObjectProperty<Color> colorProperty() {
        return color;
    }

    public DoubleProperty zOffProperty() {
        return zOff;
    }

    public double getzOff() {
        return zOff.get();
    }

    public void setzOff(double zOff) {
        this.zOff.set(zOff);
    }

    public abstract String toString();
}
