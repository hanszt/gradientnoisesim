package hzt.controller;

import hzt.model.Point3DProperty;
import hzt.model.Theme;
import hzt.model.attribute.Attribute;
import hzt.model.attribute.BrightnessAttribute;
import hzt.model.attribute.HueAttribute;
import hzt.model.attribute.SaturationAttribute;
import hzt.model.grid.EllipsePixelGrid;
import hzt.model.grid.PixelGrid;
import hzt.model.grid.RectanglePixelGrid;
import hzt.model.grid.RotationalGrid;
import hzt.model.perlin_noise.HashNoise;
import hzt.model.perlin_noise.NoiseEngine;
import hzt.model.perlin_noise.OpenSimplex1;
import hzt.model.perlin_noise.OpenSimplex2F;
import hzt.model.perlin_noise.OpenSimplex2S;
import hzt.model.perlin_noise.PerlinNoise;
import hzt.model.perlin_noise.RandomNoise;
import hzt.service.AnimationService;
import hzt.service.StatisticsService;
import hzt.service.ThemeService;
import javafx.beans.property.DoubleProperty;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.ParallelCamera;
import javafx.scene.SceneAntialiasing;
import javafx.scene.SubScene;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import java.io.IOException;

import static hzt.model.AppConstants.*;
import static hzt.model.AppConstants.Scene.ABOUT_SCENE;
import static hzt.model.AppConstants.Scene.MAIN_SCENE;

public class MainSceneController extends SceneController {
    @FXML
    private ComboBox<Attribute> attributeComboBox;
    @FXML
    private AnchorPane animationPane;
    @FXML
    private ComboBox<PixelGrid> pixelGridComboBox;
    @FXML
    private ComboBox<NoiseEngine> gradientNoiseComboBox;
    @FXML
    public ComboBox<Theme> themeCombobox;
    @FXML
    private ToggleButton fullScreenButton;
    @FXML
    private ToggleButton pauseSimButton;


    @FXML
    private ColorPicker backgroundColorPicker;
    @FXML
    private ColorPicker gridColorPicker;
    @FXML
    private Slider gradientNoiseIncrementSlider;
    @FXML
    private Slider gradientNoiseIncrementXSlider;
    @FXML
    private Slider gradientNoiseIncrementYSlider;
    @FXML
    private Slider gradientNoiseIncrementZSlider;
    @FXML
    private Slider gradientNoiseZOffSetSlider;
    @FXML
    private Label frameRateStatsLabel;
    @FXML
    private Label runTimeLabel;
    @FXML
    private Label xIncrementLabel;
    @FXML
    private Label yIncrementLabel;
    @FXML
    private Label zIncrementLabel;
    @FXML
    private Label zOffSetLabel;

    private Color backgroundColor = INIT_BG_COLOR;

    private final Group subSceneRoot = new Group();
    private final SubScene subScene2D;
    private final AnimationService animationService;
    private final ThemeService themeService = new ThemeService();

    public MainSceneController(SceneManager sceneManager) throws IOException {
        super(MAIN_SCENE.getFxmlFileName(), sceneManager);
        this.subScene2D = new SubScene(subSceneRoot, 0, 0, true, SceneAntialiasing.BALANCED);
        StatisticsService statisticsService = new StatisticsService(getGeneralStatsLabelDto(), getPixelStatsLabelDto());
        this.animationService = new AnimationService(startTimeSim, statisticsService);
        this.animationPane.getChildren().add(subScene2D);
    }

    private StatisticsService.GeneralStatsLabelDto getGeneralStatsLabelDto() {
        StatisticsService.GeneralStatsLabelDto generalStatsLabelDto = new StatisticsService.GeneralStatsLabelDto();
        generalStatsLabelDto.setFrameRateLabel(frameRateStatsLabel);
        generalStatsLabelDto.setRunTimeLabel(runTimeLabel);
        return generalStatsLabelDto;
    }

    private StatisticsService.PixelGridStatsLabelDto getPixelStatsLabelDto() {
        StatisticsService.PixelGridStatsLabelDto pixelGridStatsLabelDto = new StatisticsService.PixelGridStatsLabelDto();
        pixelGridStatsLabelDto.setXIncrementLabel(xIncrementLabel);
        pixelGridStatsLabelDto.setYIncrementLabel(yIncrementLabel);
        pixelGridStatsLabelDto.setZIncrementLabel(zIncrementLabel);
        pixelGridStatsLabelDto.setZOffsetLabel(zOffSetLabel);
        return pixelGridStatsLabelDto;
    }

    @Override
    public void setup() {
        bindFullScreenButtonToFullScreen(fullScreenButton, sceneManager.getStage());
        configureAnimationPane(animationPane);
        setInitValuesControls();
        configureSubScene(subScene2D, animationPane);
        configureComboBoxes();
        configureColorPickers();
        configureSliders();
        subSceneRoot.getChildren().add((pixelGridComboBox.getValue().getGridContainer()));
        animationService.addAnimationLoopToTimeline(initializeAnimationLoop(), initializeStatisticsLoop(), true);
    }

    private void configurePixelGrid(PixelGrid pixelGrid) {
        pixelGrid.currentNoiseEngineProperty().bind(gradientNoiseComboBox.valueProperty());
        pixelGrid.attributeProperty().bind(attributeComboBox.valueProperty());
        pixelGrid.setzOff(INIT_GRADIENT_NOISE_OFFSET_Z);
        pixelGrid.colorProperty().bind(gridColorPicker.valueProperty());
        Point3DProperty increment = pixelGrid.getIncrement();
        increment.xProperty().bindBidirectional(gradientNoiseIncrementXSlider.valueProperty());
        increment.yProperty().bindBidirectional(gradientNoiseIncrementYSlider.valueProperty());
        increment.zProperty().set(gradientNoiseIncrementZSlider.getValue());
    }

    private static void configureSubScene(SubScene subScene, Pane animationPane) {
        subScene.widthProperty().bind(animationPane.widthProperty());
        subScene.heightProperty().bind(animationPane.heightProperty());
        subScene.setCamera(new ParallelCamera());
    }

    private void configureAnimationPane(AnchorPane animationPane) {
        animationPane.setPrefSize(INIT_ANIMATION_PANE_DIMENSION.getWidth(), INIT_ANIMATION_PANE_DIMENSION.getHeight());
        animationPane.setBackground(new Background(new BackgroundFill(backgroundColor, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    private static void bindFullScreenButtonToFullScreen(ToggleButton fullScreenButton, Stage stage) {
        stage.fullScreenProperty().addListener((o, c, isFullScreen) -> fullScreenButton.setSelected(isFullScreen));
        stage.addEventFilter(KeyEvent.KEY_PRESSED, key -> setFullScreen(stage, key));
    }

    private static void setFullScreen(Stage stage, KeyEvent key) {
        if (key.getCode() == KeyCode.F11) {
            stage.setFullScreen(!stage.isFullScreen());
        }
    }

    private void configureComboBoxes() {
        NoiseEngine openSimplexNoise = new OpenSimplex2F();
        gradientNoiseComboBox.getItems().addAll(openSimplexNoise,
                new OpenSimplex2S(), new OpenSimplex1(), new PerlinNoise(), new HashNoise(), new RandomNoise());
        gradientNoiseComboBox.setValue(openSimplexNoise);

        PixelGrid rectanglePixelGrid = new RectanglePixelGrid();
        pixelGridComboBox.getItems().addAll(rectanglePixelGrid, new RotationalGrid(), new EllipsePixelGrid());
        pixelGridComboBox.setValue(rectanglePixelGrid);
        pixelGridComboBox.getItems().forEach(this::configurePixelGrid);

        Attribute brightnessAttribute = new BrightnessAttribute();
        attributeComboBox.getItems().addAll(brightnessAttribute, new SaturationAttribute(), new HueAttribute());
        attributeComboBox.setValue(brightnessAttribute);

        themeService.getThemes().forEach(theme -> themeCombobox.getItems().add(theme));
        themeService.currentThemeProperty().bind(themeCombobox.valueProperty());
        themeService.styleSheetProperty().addListener(this::changeStyle);
        themeCombobox.setValue(ThemeService.DARK_THEME);
    }

    public void changeStyle(ObservableValue<? extends String> o, String c, String newVal) {
        for (SceneController sceneController : sceneManager.getSceneControllerMap().values()) {
            ObservableList<String> styleSheets = sceneController.scene.getStylesheets();
            styleSheets.removeIf(filter -> !styleSheets.isEmpty());
            if (newVal != null) {
                styleSheets.add(newVal);
            }
        }
    }

    private EventHandler<ActionEvent> initializeAnimationLoop() {
        return loop -> AnimationService.run(pixelGridComboBox.getValue());
    }

    private EventHandler<ActionEvent> initializeStatisticsLoop() {
        return loop -> animationService.runStatistics(pixelGridComboBox.getValue());
    }

    private void configureColorPickers() {
        backgroundColorPicker.setValue(INIT_BG_COLOR);
        gridColorPicker.setValue(INIT_GRID_COLOR);
    }

    private void configureSliders() {
        gradientNoiseIncrementSlider.valueProperty().addListener(this::setXAndYSliderValues);
        gradientNoiseIncrementXSlider.valueProperty().addListener((o, c, newVal) ->
                pixelGridComboBox.getValue().updateNoiseXOffsetAllPixels(newVal.doubleValue()));
        gradientNoiseIncrementYSlider.valueProperty().addListener((o, c, newVal) ->
                pixelGridComboBox.getValue().updateNoiseYOffsetAllPixels(newVal.doubleValue()));
        pixelGridComboBox.getItems()
                .forEach(g -> g.zOffProperty().bindBidirectional(gradientNoiseZOffSetSlider.valueProperty()));
        gradientNoiseIncrementZSlider.valueProperty().addListener(this::setIncrementZProperty);
    }

    private void setXAndYSliderValues(ObservableValue<? extends Number> o, Number c, Number newVal) {
        final var pixelGrid = pixelGridComboBox.getValue();
        final var newValue = newVal.doubleValue();
        pixelGrid.getIncrement().setX(newValue);
        pixelGrid.getIncrement().setY(newValue);
    }

    private void setIncrementZProperty(ObservableValue<? extends Number> o, Number c, Number newVal) {
        double newZIncrement = pauseSimButton.isSelected() ? 0 : newVal.doubleValue();
        pixelGridComboBox.getValue().getIncrement().setZ(newZIncrement);
    }

    private void setInitValuesControls() {
        double initIncrementX = INIT_GRADIENT_INCREMENT_VECTOR.getX();
        double initIncrementY = INIT_GRADIENT_INCREMENT_VECTOR.getY();
        gradientNoiseIncrementSlider.setValue((initIncrementX + initIncrementY) / 2);
        gradientNoiseIncrementXSlider.setValue(initIncrementX);
        gradientNoiseIncrementYSlider.setValue(initIncrementY);
        gradientNoiseIncrementZSlider.setValue(INIT_GRADIENT_INCREMENT_VECTOR.getZ());
        final var INIT_Z_OFFSET = 50;
        gradientNoiseZOffSetSlider.setValue(INIT_Z_OFFSET);
    }

    @FXML
    private void backgroundColorPickerAction(ActionEvent event) {
        backgroundColor = ((ColorPicker) event.getSource()).getValue();
        animationPane.setBackground(new Background(new BackgroundFill(backgroundColor, CornerRadii.EMPTY, Insets.EMPTY)));
    }

    @FXML
    private void pauseSimButtonAction(ActionEvent actionEvent) {
        DoubleProperty zIncrement = pixelGridComboBox.getValue().getIncrement().zProperty();
        if (((ToggleButton) actionEvent.getSource()).isSelected()) {
            animationService.pauseAnimationTimeline();
            zIncrement.set(0);
        } else {
            animationService.startAnimationTimeline();
            zIncrement.set(gradientNoiseIncrementZSlider.getValue());
        }
    }

    @FXML
    private void resetButtonAction() {
        setInitValuesControls();
        pixelGridComboBox.getItems().forEach(PixelGrid::reset);
    }

    @FXML
    private void transparentButtonAction(ActionEvent actionEvent) {
        boolean transparent = ((ToggleButton) actionEvent.getSource()).isSelected();
        sceneManager.getStage().setOpacity(transparent ? STAGE_OPACITY : 1);
    }

    @FXML
    private void fullScreenButtonAction(ActionEvent actionEvent) {
        sceneManager.getStage().setFullScreen(((ToggleButton) actionEvent.getSource()).isSelected());
    }

    @FXML
    private void pixelGridComboBoxAction(ActionEvent actionEvent) {
        ComboBox<?> comboBox = (ComboBox<?>) actionEvent.getSource();
        subSceneRoot.getChildren().removeIf(Group.class::isInstance);
        subSceneRoot.getChildren().add(((PixelGrid) comboBox.getValue()).getGridContainer());
    }

    @FXML
    private void noZIncrement() {
        gradientNoiseIncrementZSlider.setValue(0);
    }

    @FXML
    private void showAbout() {
        sceneManager.setupScene(ABOUT_SCENE);
    }

    protected SceneController getBean() {
        return this;
    }
}
