package hzt.controller;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalTime;

import static hzt.model.AppConstants.*;

public abstract class SceneController extends FXMLController {

    private static final String FXML_FILE_LOCATION = "../../fxml/";

    private boolean setup;

    private final String fxmlFileName;
    protected final SceneManager sceneManager;
    protected final Scene scene;
    protected final LocalTime startTimeSim;

    public SceneController(String fxmlFileName, SceneManager sceneManager) throws IOException {
        this.startTimeSim = LocalTime.now();
        this.fxmlFileName = fxmlFileName;
        this.sceneManager = sceneManager;
        FXMLLoader fxmlLoader = new FXMLLoader();
        setControllerFactory(fxmlLoader);
        Parent root = fxmlLoader.load();
        scene = new Scene(root, INIT_SCENE_DIMENSION.getWidth(), INIT_SCENE_DIMENSION.getHeight());
    }

    //to be able to pass arguments to the constructor, it's necessary to specify the controller in the controller factory of the loader
    private void setControllerFactory(FXMLLoader loader) {
        loader.setControllerFactory(c -> getBean());
        loader.setLocation(getClass().getResource(FXML_FILE_LOCATION + fxmlFileName));
    }

    protected abstract Object getBean();

    public abstract void setup();

    @FXML
    public void newInstance() {
       new AppManager(new Stage()).start();
    }

    @FXML
    void quitInstance() {
        sceneManager.getStage().close();
    }

    @FXML
    void exitProgram() {
        System.exit(0);
    }

    public boolean isSetup() {
        return setup;
    }

    public void setSetup(boolean setup) {
        this.setup = setup;
    }
}
