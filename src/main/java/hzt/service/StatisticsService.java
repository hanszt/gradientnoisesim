package hzt.service;

import hzt.model.grid.PixelGrid;
import javafx.animation.AnimationTimer;
import javafx.scene.control.Label;
import javafx.util.Duration;
import lombok.Getter;
import lombok.Setter;

import static hzt.model.AppConstants.INIT_FRAME_RATE;
import static java.lang.String.format;

public class StatisticsService {

    private final GeneralStatsLabelDto generalStatsDto;
    private final PixelGridStatsLabelDto pixelGridStatsLabelDto;
    private final SimpleFrameRateMeter frameRateMeter = new SimpleFrameRateMeter();

    public StatisticsService(GeneralStatsLabelDto generalStatsDto, PixelGridStatsLabelDto pixelGridStatsLabelDto) {
        this.generalStatsDto = generalStatsDto;
        this.pixelGridStatsLabelDto = pixelGridStatsLabelDto;
        frameRateMeter.initialize();
    }

    private static final String TWO_DEC_DOUBLE = "%-4.2f";

    public void showGlobalStatistics(Duration runTimeSim) {
        generalStatsDto.getFrameRateLabel().setText(format(TWO_DEC_DOUBLE + " f/s", frameRateMeter.frameRate));
        generalStatsDto.getRunTimeLabel().setText(format("%-4.3f seconds", runTimeSim.toSeconds()));
    }

    public void showPixelGridStatistics(PixelGrid pixelGrid) {
        pixelGridStatsLabelDto.getXIncrementLabel().setText(format(TWO_DEC_DOUBLE, pixelGrid.getIncrement().getX()));
        pixelGridStatsLabelDto.getYIncrementLabel().setText(format(TWO_DEC_DOUBLE, pixelGrid.getIncrement().getY()));
        pixelGridStatsLabelDto.getZIncrementLabel().setText(format(TWO_DEC_DOUBLE + " units / s", pixelGrid.getIncrement().getZ()));
        pixelGridStatsLabelDto.getZOffsetLabel().setText(format(TWO_DEC_DOUBLE + " units", pixelGrid.getzOff()));
    }

    private static class SimpleFrameRateMeter {

        private final long[] frameTimes = new long[100];
        private int frameTimeIndex = 0;
        private boolean arrayFilled = false;
        private double frameRate = INIT_FRAME_RATE;

        private void initialize() {
            AnimationTimer frameRateTimer = new AnimationTimer() {

                @Override
                public void handle(long now) {
                    long oldFrameTime = frameTimes[frameTimeIndex];
                    frameTimes[frameTimeIndex] = now;
                    frameTimeIndex = (frameTimeIndex + 1) % frameTimes.length;
                    if (frameTimeIndex == 0) arrayFilled = true;
                    if (arrayFilled) {
                        long elapsedNanos = now - oldFrameTime;
                        long elapsedNanosPerFrame = elapsedNanos / frameTimes.length;
                        frameRate = 1e9 / elapsedNanosPerFrame;
                    }
                }
            };
            frameRateTimer.start();
        }

    }

    @Getter
    @Setter
    public static class PixelGridStatsLabelDto {

        private Label zOffsetLabel;
        private Label xIncrementLabel;
        private Label yIncrementLabel;
        private Label zIncrementLabel;

        public PixelGridStatsLabelDto() {
            super();
        }

    }

    @Getter
    @Setter
    public static class GeneralStatsLabelDto {

        private Label frameRateLabel;
        private Label runTimeLabel;

        public GeneralStatsLabelDto() {
            super();
        }
    }
}
