package hzt.service;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Collectors;

public class AboutService {

    private static final Logger LOGGER = LoggerFactory.getLogger(AboutService.class);
    private static final String RELATIVE_TEXT_RESOURCE_DIR = "../../about";

    public List<AboutText> loadContent() {
        List<AboutText> aboutTexts = new ArrayList<>();
        final var url = getClass().getResource(RELATIVE_TEXT_RESOURCE_DIR);
        if (url != null) {
            File fileDir = new File(url.getFile());
            if (fileDir.isDirectory()) {
                final var fileNames = Objects.requireNonNullElse(fileDir.listFiles(), new File[]{});
                for (File file : fileNames) {
                    String name = file.getName().replace(".txt", "").replace("_", " ");
                    AboutText aboutText = new AboutText(name, new SimpleStringProperty(loadTextContent(file)));
                    aboutTexts.add(aboutText);
                }
            }
        } else {
            LOGGER.error("about folder not found...");
        }
        if (aboutTexts.isEmpty()) {
            aboutTexts.add(new AboutText("no content", new SimpleStringProperty()));
        }
        return aboutTexts;
    }

    private static String loadTextContent(File file) {
        return readInputFileByLine(file).stream().collect(Collectors.joining(System.lineSeparator()));
    }

    private static List<String> readInputFileByLine(File file) {
        List<String> inputList = new ArrayList<>();
        try (Scanner input = new Scanner(file)) {
            while (input.hasNextLine()) {
                inputList.add(input.nextLine());
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("File {} not found...", file, e);
        }
        return inputList;
    }

    public record AboutText(String title, StringProperty text) {

        @Override
        public String toString() {
            return title;
        }
    }
}
