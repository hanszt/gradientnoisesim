package hzt.service;

import hzt.model.grid.PixelGrid;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.util.Duration;

import java.time.LocalTime;

import static hzt.model.AppConstants.INIT_FRAME_DURATION;
import static javafx.animation.Animation.INDEFINITE;

public final class AnimationService {

    private final LocalTime startTimeSim;
    private final StatisticsService statisticsService;
    private final Timeline animationTimeline;
    private final Timeline statisticsTimeline;

    public AnimationService(LocalTime startTimeSim, StatisticsService statisticsService) {
        this.startTimeSim = startTimeSim;
        this.statisticsService = statisticsService;
        this.animationTimeline = setupTimeLine();
        this.statisticsTimeline = setupTimeLine();
    }

    private static Timeline setupTimeLine() {
        Timeline t = new Timeline();
        t.setCycleCount(INDEFINITE);
        return t;
    }

    public void addAnimationLoopToTimeline(EventHandler<ActionEvent> animationLoop,
                                           EventHandler<ActionEvent> statisticsLoop, boolean start) {
        animationTimeline.getKeyFrames().add(new KeyFrame(INIT_FRAME_DURATION, "Animation keyframe", animationLoop));
        statisticsTimeline.getKeyFrames().add(new KeyFrame(INIT_FRAME_DURATION, "Statistics keyframe", statisticsLoop));
        if (start) {
            animationTimeline.play();
            statisticsTimeline.play();
        }
    }

    public static void run(PixelGrid gradientNoisePixelGrid) {
        gradientNoisePixelGrid.updateNoiseOffsetAllPixels();
    }

    public void runStatistics(PixelGrid gradientNoisePixelGrid) {
        Duration runTimeSim = Duration.millis((LocalTime.now().toNanoOfDay() - startTimeSim.toNanoOfDay()) / 1e6);
        statisticsService.showGlobalStatistics(runTimeSim);
        statisticsService.showPixelGridStatistics(gradientNoisePixelGrid);
    }

    public void startAnimationTimeline() {
        animationTimeline.play();
    }

    public void pauseAnimationTimeline() {
        animationTimeline.pause();
    }

}
