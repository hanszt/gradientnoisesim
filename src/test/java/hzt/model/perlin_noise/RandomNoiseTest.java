package hzt.model.perlin_noise;

import org.junit.jupiter.api.Test;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RandomNoiseTest {

    @Test
    void testNoiseSameInputSameOutput() {
        final var randomNoise = new RandomNoise();

        final var grouping = IntStream.range(0, 100)
                .mapToObj(i -> randomNoise.noise(3, 234.32, Math.PI))
                .collect(Collectors.groupingBy(noise -> noise));

        grouping.keySet().forEach(System.out::println);

        assertEquals(1, grouping.size());
    }

}
