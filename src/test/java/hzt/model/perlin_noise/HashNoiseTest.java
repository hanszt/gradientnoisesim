package hzt.model.perlin_noise;

import org.junit.jupiter.api.Test;

import java.util.Objects;

import static org.junit.jupiter.api.Assertions.assertEquals;

class HashNoiseTest {

    @Test
    void testHashOfThreeDoubles() {
        int hash = Objects.hash(234.9,2.7,2234.5);
        System.out.println("a = " + hash);
        assertEquals(-2063368835, hash);
    }
}
