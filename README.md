# Gradient Noise Visualisation

A program to visualize different noise algorithms.

These algorithms can be used to facilitate computer terrain generation or other computer graphics works of art.

Made By Hans Zuidervaart.

## Motivation
I was curious about the workings of noise algorithms!

## Screenshots
#### screenshot 1: A preview of the application
![NoiseSim](src/main/resources/images/example1.png)

#### screenshot 2: 
![NoiseSim](src/main/resources/images/example2.png)

#### screenshot 3: 
![NoiseSim](src/main/resources/images/example3.png)

## Background
Gradient noise is a type of noise commonly used as a procedural texture primitive in computer graphics. It is conceptually different[further explanation needed], and often confused with value noise. This method consists of a creation of a lattice of random (or typically pseudorandom) gradients, dot products of which are then interpolated to obtain values in between the lattices. An artifact of some implementations of this noise is that the returned value at the lattice points is 0. Unlike the value noise, gradient noise has more energy in the high frequencies.

The first known implementation of a gradient noise function was Perlin noise, credited to Ken Perlin, who published the description of it in 1985. [1] Later developments were Simplex noise and OpenSimplex noise. 

## Setup
Before you run the program, make sure you modify the following in Intellij:
- Go to File -->  Settings --> Appearance and Behaviour --> Path Variables
- Add a new line variable by clicking active the + sign and name it PATH_TO_FX and browse to the lib folder of the JavaFX SDK to set its value, and click apply. 
- Go to Run --> Edit Configurations
- Type the following in the VM Options section: --module-path ${PATH_TO_FX} --add-modules javafx.controls,javafx.fxml
- Click Apply and Run the application. It should work now.
    
source: https://openjfx.io/openjfx-docs/#IDE-Intellij

## Sources
[OpenSimplexNoise github](https://gist.github.com/KdotJPG/b1270127455a94ac5d19)
[Perlin noise sourcecode](https://mrl.cs.nyu.edu/~perlin/experiments/packing/render/Noise.java)
[The coding train](https://www.youtube.com/watch?v=BjoM9oKOAKY)